# README #

Completed Assignment task. IN this repository have two directory.
1 : Assignment for reactjs code (frontend).
2 : Assignment_api for nodejs code (api).

### How to set up task? ###

* Take a git clone repository.
* Open MongoDB Compass.
* Create "db_assignment_project" database.
* Import users.json and tasks.json file in "db_assignment_project" database with the help of MongoDB Compass.
* Go to the Assignment_api directory.
* Install node modules using below command.
#### npm install ####
* After installed completed node modules run the below command.
#### npm run dev ####
* Node api code will run on http://localhost:3006
* Go to the Assignment directory.
* Install node modules using below command.
#### npm install ####
* After installed completed node modules run the below command.
#### npm run start ####
* A new window will open and redirect to the login page.
* Use below login credentials for admin.
#### Username : ashwin@mailinator.com ####
#### Pasword : Admin420! ####