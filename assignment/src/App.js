import React, { Component, Fragment } from "react";
import Notification from 'react-notify-toast';
import { Router } from "./Routes";
import { HeaderComponent } from "./Components";
import { AuthHelper } from "./Helpers";

class App extends Component {
  render () {
    return (
      <Fragment>
        <Notification/>
        {/* <TitleComponent/> */}
        {
          AuthHelper.getAuth() &&
          <HeaderComponent/>
        }
        <Router/>
      </Fragment>
    )
  }
}

export default App;
