import React, { Component, Fragment } from 'react';
import config from "../../config";
import { Auth } from "../../Services";
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavbarText } from 'reactstrap';

class HeaderComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    }
    this.menuToggle = this.menuToggle.bind(this);
  }

  menuToggle = () => {
    const { isOpen } = this.state;
    this.setState({isOpen: !isOpen});
  };

  logout = () => {
    Auth.logout()
    window.location.href = '/login';
  }

  render() {
    const { isOpen } = this.state;
    return (
      <Fragment>
        <Navbar  className="navbar navbar-dark bg-dark" expand="md">
        <NavbarBrand href="/">{config.cognito.WEB_SITE_NAME}</NavbarBrand>
        <NavbarToggler onClick={this.menuToggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar activeKey={window.location.pathname}>
            <NavItem>
              <NavLink href="/dashboard">Dashboard</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/users">Users</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/tasks">Task</NavLink>
            </NavItem>
          </Nav>
          <NavbarText onClick={() => this.logout()}>Logo Out</NavbarText>
        </Collapse>
      </Navbar>
      </Fragment>
    )
  }
}

export default HeaderComponent;