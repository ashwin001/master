import React, { Component } from 'react';
import Helmet from 'react-helmet';
import config from "../../config";

class TitleComponent extends Component {
  render() {
    const { title } = this.props;
    return (
      <Helmet>
        <title>{config.cognito.WEB_SITE_NAME +(title ? ' | '+ title : '')}</title>
      </Helmet>
    )
  }
}

export default TitleComponent;