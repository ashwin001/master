// Config
import config from "../../config";
const SUCCESS_CODE = 200;

const API = {
    // AUTH
    LOGIN: `${config.apiURL.URL}/login`,
    LOGOUT: `${config.apiURL.URL}/logout`,

    //DASHBOARD
    GET_DASHBOARD_INFO: `${config.apiURL.URL}/dashboard`,
    //USER
    GET_ALL_USER_LIST: `${config.apiURL.URL}/users`,
    ADD_USER: `${config.apiURL.URL}/user`,
    EDIT_USER: `${config.apiURL.URL}/user`,
    DELETE_USER: `${config.apiURL.URL}/user`,
    //TASK
    GET_ALL_TASK_LIST: `${config.apiURL.URL}/tasks`,
    ADD_TASK: `${config.apiURL.URL}/task`,
    EDIT_TASK: `${config.apiURL.URL}/task`,
    DELETE_TASK: `${config.apiURL.URL}/task`,
}

export default {
    SUCCESS_CODE,
    ...API
};
