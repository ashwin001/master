const CONSTANT =  {
  ITEMS_PERPAGE: 10,
  ACCOUNT_TYPE: {
    1: "Admin",
    2: "Manager",
    3: "Developer",
  },
  SKILLS: {
    1: "PHP",
    2: "Laravel",
    3: "Codeigniter",
    4: "Angular",
    5: "ReactJS",
    6: "NodeJS",
    7: "JavaScript",
    8: "JQuery",
    9: "Ajax",
    10: "HTML",
    11: "CSS",
    12: "SCSS",
    13: "IOS",
    14: "Android",
    15: "React Native",
    16: "Rest API",
    17: "MySQL",
    18: "MongoDB"
  }
}

export default CONSTANT;
