// wrapper for localstorage and auth object setting

export function getAuth() {
  const auth = localStorage.getItem('user_auth');
  if (!auth) {
    return null;
  }
  return JSON.parse(auth);
}

export function setAuth(auth) {
  localStorage.setItem('user_auth', JSON.stringify(auth));
}

export function removeAuth() {
  localStorage.removeItem('user_auth');
}

export default {getAuth, setAuth, removeAuth};