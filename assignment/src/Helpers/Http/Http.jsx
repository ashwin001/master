import { notify } from 'react-notify-toast';
import { AuthHelper } from "../../Helpers";

function status(res) {
  if (res.status >= 200 && res.status < 400) {
    return Promise.resolve(res);
  } else if (res.status === 401) {
    localStorage.removeItem('userDetails')
    window.location.replace('/login')
  }
  return Promise.reject(res);
}

function json(res) {
  if (
    res &&
    res.headers &&
    res.headers.get("content-type").startsWith("application/json")
  ) 
    return res.json().then(response=>{
      return response;
    });

  return {};
}

function parseErrorMessage(response) {
  var errorMsg = (response.MESSAGE && response.MESSAGE != "" )? response.MESSAGE : response.global_error;
  if (errorMsg == "") {
    for (var key in response.error) {
      errorMsg = response.error[key];
      if (errorMsg != "") {
        break;
      }
    }
  }
  return errorMsg
}

function error(res, errorNotifyHide) {
  if (
    res &&
    res.headers &&
    res.headers.get("content-type").startsWith("application/json")
  ) {
    return res.json().then(err => {
      if(!errorNotifyHide) {
        notify.show(parseErrorMessage(err), "error", 2500);
      }
      throw err;
    });
  }

  if (res) {
    throw res;
  }

  return {};
}

export function get(url) {
  const auth = AuthHelper.getAuth();
  const token = (!!auth && auth) || null;
  if (!token)
    return;
  const settings = {
    headers: {
      "Content-Type": "application/json",
      auth: token,
    }
  };
  return fetch(url, settings)
    .then(status)
    .then(json)
    .catch(error);
}

export function post(url, body = {}, errorNotifyHide = false) {
  const auth = AuthHelper.getAuth();
  const token = (!!auth && auth) || null;
  if (!token)
    return;

  const settings = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json",
      auth: token
    }
  };

  return fetch(url, settings)
    .then(status)
    .then(json)
    .catch((res) => error(res, errorNotifyHide));
}

export function put(url, body = {}, errorNotifyHide = false) {
  const auth = AuthHelper.getAuth();
  const token = (!!auth && auth) || null;
  if (!token)
    return;

  const settings = {
    method: "PUT",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json",
      auth: token
    }
  };

  return fetch(url, settings)
    .then(status)
    .then(json)
    .catch((res) => error(res, errorNotifyHide));
}

export function delete_request(url, errorNotifyHide = false) {
  const auth = AuthHelper.getAuth();
  const token = (!!auth && auth) || null;
  if (!token)
    return;
  const settings = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      auth: token
    }
  };

  return fetch(url, settings)
    .then(status)
    .then(json)
    .catch((res) => error(res, errorNotifyHide));
}


export default { get, post, delete_request, put};