import moment from 'moment';
const Utils = {
    DobFormat: (date) => {
        return moment(date).format('MMM DD, YYYY')
    },
    CalculateAge: (date) => {
        return moment().diff(date, "years")
    },
    DueDateGraterThanCurrentDate: (date) => {
        return moment().diff(date, "days")
    }
}

export default { ...Utils }