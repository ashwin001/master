import validator from "validator";
import _ from 'lodash';

class Validation {
    static init(type, value) {
        let isValidate = null;
        if (value !== '' && value !== null && (value.length >= 0 || Date.parse(value))) {
            switch (type) {
                case "required":
                    isValidate = validator.isEmpty(value) ? true : false;
                    break;
                case "email_address":
                    isValidate = validator.isEmail(value) ? false : true;
                    break;
                case "password":
                case "confirm_password":
                    isValidate = value.length >= 6 && value.length <= 25 ? false : true;
                    break;
                case "date":
                    var someDate = Date.parse(value);
                    isValidate = isNaN(someDate) ? true : false;
                    break;
                case "first_name":
                case "last_name":
                    isValidate =
                        validator.isLength(value, { min: 3, max: 25 }) &&
                            value.match(/(([A-Za-z'-]+)?)$/)
                            ? false
                            : true;
                    break;
                case "skills":
                    isValidate = value.length < 1 ? false : true;
                default:
                    break;
            }
        } else {
            isValidate = false
        }
        return isValidate;
    }


    static isFormValid(forms, state) {
        let arr = []
        _.map(forms, (item) => {
            let itemValidate = this.init(item, state[item]) === false;
            if (itemValidate) {
                arr.push(itemValidate);
            }
        })
        return arr.length === forms.length;
    }
}

export default Validation;