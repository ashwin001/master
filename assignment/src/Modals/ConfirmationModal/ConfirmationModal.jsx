import React, { Component, Fragment } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Http } from "../../Helpers";
class ConfirmationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      formValid: false,
    }
  }

  render() {
    const { isOpen, toggle, itemProps, message, title, callback } = this.props
    const ModalProps = {
      isOpen,
      toggle,
      size: 'md',
      className: 'custom-modal'
    }

    return (
      <Modal {...ModalProps}>
        <span className="float-right position-absolute" style={{right: "20px", fontSize: "30px", cursor: "pointer"}} onClick={toggle}>&times;</span>
        <ModalHeader className="w-100">
            {title}
        </ModalHeader>
        <ModalBody>
          {message}
        </ModalBody>
        <ModalFooter>
          <Button className="btn btn-info" onClick={() => toggle()} >No</Button>
          <Button className="btn btn-danger" onClick={() => { callback() }}>Yes</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

ConfirmationModal.defaultProps = {
  isOpen: true,
  toggle: () => { }
}
export default ConfirmationModal;
