import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthHelper } from "../../Helpers";

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        AuthHelper.getAuth()
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)