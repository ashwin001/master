import React, { Component, Fragment } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { Login, Dashboard, Users, AddUser, EditUser, Tasks, AddTask, EditTask } from "../../Views";
import { PrivateRoute } from "../PrivateRoute/PrivateRoute";

class Router extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isUserLogggedIn: false,
        }

    }
    componentDidMount() {
        this.setState({ isUserLoggedIn: !!localStorage.getItem("isUserLoggedIn") });
    }
    render() {
        const { isUserLogggedIn } = this.state;
        return (
            <Fragment>
                <BrowserRouter>
                    <Switch>
                        <Route exact  path="/login" component={Login}/>
                        <PrivateRoute  path="/dashboard" component={Dashboard}/>
                        <PrivateRoute  path="/users" component={Users}/>
                        <PrivateRoute  path="/add_user" component={AddUser}/>
                        <PrivateRoute  path="/edit_user" component={EditUser}/>
                        <PrivateRoute  path="/tasks" component={Tasks}/>
                        <PrivateRoute  path="/add_task" component={AddTask}/>
                        <PrivateRoute  path="/edit_task" component={EditTask}/>
                        <Redirect from="*" to='/login' />
                    </Switch>
                </BrowserRouter>
            </Fragment>
        );
    }
}

export default Router;
