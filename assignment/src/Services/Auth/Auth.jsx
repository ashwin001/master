import { BehaviorSubject } from 'rxjs';
import { API } from "../../Constants";
import { AuthHelper, Http } from '../../Helpers';
import { notify } from 'react-notify-toast';


const loggedUserDetails = new BehaviorSubject(JSON.parse(localStorage.getItem('userDetails')));

function login(email_address, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      email_address: email_address,
      password: password,
    })
  };
  return fetch(API.LOGIN, requestOptions)
    .then((res) => res.json())
    .then(data => {
      if (data.STATUS === "FAILURE")
      { 
        notify.show(data.MESSAGE, "error", 2500);
        return data;
      }
      else
      {
        AuthHelper.setAuth(data.DATA.access_token)
        localStorage.setItem('userDetails', JSON.stringify(data.DATA));
        loggedUserDetails.next(data.DATA);
        return data.DATA;
      }
    }, error=> {
      notify.show(error.MESSAGE, "error", 2500);
      return error;
    });
}

function logout() {
  // remove user from local storage to log user out
  let param = { access_token: AuthHelper.getAuth() }
  Http.post(API.LOGOUT, param)
  .then((response) => {
    localStorage.clear();
    AuthHelper.removeAuth();
    loggedUserDetails.next(null);
  }).catch(error => {

  });
}

export default {
  login,
  logout,
  loggedUser: loggedUserDetails.asObservable(),
  get loggedUserValue() { return loggedUserDetails.value }
}