import React, { Component, Fragment } from "react";
import { Container, Row, Col, Card, CardBody, CardTitle, CardSubtitle } from "reactstrap";
import { API } from "../../Constants";
import { Http } from "../../Helpers";
import { Auth } from "../../Services";
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      totalManagers: 0,
      totalDevelopers: 0,
      totalTasks: 0,
    }
  }

  getTotalNumberOfManagers = () => {
    let param = {
      account_type: 2
    };
    this.setState({ posting: true });

    Http.post(API.GET_ALL_USER_LIST, param)
      .then((response) => {
        this.setState({
          totalManagers: response.DATA.length,
          posting: false,
        });
      })
      .catch((error) => {
        this.setState({ totalManagers: 0, posting: false });
      });
  };

  getTotalNumberOfDevelopers = () => {
    let param = {
      account_type: 3
    };
    this.setState({ posting: true });

    Http.post(API.GET_ALL_USER_LIST, param)
      .then((response) => {
        this.setState({
          totalDevelopers: response.DATA.length,
          posting: false,
        });
      })
      .catch((error) => {
        this.setState({ totalDevelopers: 0, posting: false });
      });
  };

  getTotalNumberOfTasks = () => {
    this.setState({ posting: true });

    Http.get(API.GET_ALL_TASK_LIST)
      .then((response) => {
        this.setState({
          totalTasks: response.DATA.length,
          posting: false,
        });
      })
      .catch((error) => {
        this.setState({ totalTasks: 0, posting: false });
      });
  };

  componentDidMount() {
    if (Number(Auth.loggedUserValue.account_type) == 1)
      this.getTotalNumberOfManagers();

    this.getTotalNumberOfDevelopers();
    this.getTotalNumberOfTasks();
  }

  render() {
    const { totalManagers, totalDevelopers, totalTasks} = this.state;
    return (
      <Fragment>
        <Container className="my-5">
          <Row>
            <Col><h1>Dashboard</h1></Col>
          </Row>
          <Row>
            {
              (Number(Auth.loggedUserValue.account_type) == 1) &&
                <Col>
                  <Card>
                    <CardBody>
                      <CardTitle tag="h5">Total Managers</CardTitle>
                      <CardSubtitle tag="h6">{totalManagers}</CardSubtitle>
                    </CardBody>
                  </Card>
                </Col>
            }
            <Col>
              <Card>
                <CardBody>
                  <CardTitle tag="h5">Total Developers</CardTitle>
                  <CardSubtitle tag="h6">{totalDevelopers}</CardSubtitle>
                </CardBody>
              </Card>
            </Col>
            <Col>
              <Card>
                <CardBody>
                  <CardTitle tag="h5">Total Tasks</CardTitle>
                  <CardSubtitle tag="h6">{totalTasks}</CardSubtitle>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}
export default Dashboard;
