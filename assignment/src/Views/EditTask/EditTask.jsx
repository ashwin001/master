import React, { Component, Fragment } from "react";
import { Container, Card, CardBody, Row, Col, Form, FormGroup, Label, Input, FormFeedback, Button } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import { notify } from 'react-notify-toast';
import { Validation, Http } from "../../Helpers";
import { API, CONSTANT } from "../../Constants";
import _ from "lodash";
const animatedComponents = makeAnimated();
class EditTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      formValid: false,
      _id: this.props.location.state.taskData._id,
      title: this.props.location.state.taskData.title,
      description: this.props.location.state.taskData.description,
      due_date: new Date(this.props.location.state.taskData.due_date),
      skills: this.props.location.state.taskData.skills,
      assign_to: this.props.location.state.taskData.assign_to,
      start_date: new Date,
      skill_options: [],
      assign_to_options: [],
      skill_default: [],
      assign_to_default: [],
    }
    _.map(CONSTANT.SKILLS, (skill, index) => {
      this.state.skill_options.push({value: index, label: skill})
    })
    _.map(this.props.location.state.taskData.skills, (skill) => {
      this.state.skill_options.find(s => {
        if (Number(skill) === Number(s.value))
          this.state.skill_default.push(s);
      })
    })
    this.getUserList();
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSkillChange = this.handleSkillChange.bind(this);
    this.handleAssignToChange = this.handleAssignToChange.bind(this);
  }

  getUserList = () => {
    this.setState({ posting: true });

    Http.get(API.GET_ALL_USER_LIST)
      .then((response) => {        
        _.map(response.DATA, (user, index) => {
          this.state.assign_to_options.push({value: user._id, label: user.name.first_name+" "+user.name.last_name});          
          if (this.props.location.state.taskData.assign_to.indexOf(user._id) > -1)
            this.state.assign_to_default.push({value: user._id, label: user.name.first_name+" "+user.name.last_name});
        })
        this.setState({ posting: false});
      })
      .catch((error) => {
        this.setState({ users: [], posting: false });
      });
  };

  handleAssignToChange = (assigns_to) => {
    let assignToArray = [];
    _.map(assigns_to, (assign_to) => {
      assignToArray.push(assign_to.value)
    })
    this.setState({ assign_to: assignToArray }, () => { this.validateForm() });
  }

  handleSkillChange = (skills) => {
    let skillArray = [];
    _.map(skills, (skill) => {
      skillArray.push(skill.value)
    })
    this.setState({ skills: skillArray }, () => { this.validateForm() });
  }

  handleDateChange (date) {
    this.setState({due_date: date});
  }

  handleChange = (e) => {
    const name = e.target.name;
    let value = e.target.value;
    this.setState({ [name]: value }, this.validateForm);
  };

  validateForm() {
    const { title, description, due_date, skills, assign_to } = this.state;
    this.setState({
      formValid:
        title !== "" &&
        description !== "" &&
        due_date !== "" &&
        skills.length > 0 &&
        assign_to.length > 0 &&
        (Validation.init("required", title) === false) &&
        (Validation.init("required", description) === false) &&
        (Validation.init("date", due_date) === false)
    });
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { _id, title, description, due_date, skills, assign_to } = this.state;
    let param = { title, description, due_date, skills, assign_to }
    this.setState({ posting: true });
    Http.put(API.EDIT_TASK+'/'+_id, param)
      .then((response) => {
        this.setState({ posting: false });
        notify.show(response.MESSAGE, "success", 5000, { background: '#0E1717', text: "#FFFFFF" });
        this.props.history.push("/tasks");
      })
      .catch(() => {
        this.setState({ posting: false });
      });
  }

  render() {
    const { title, description, due_date, skills, assign_to, formValid, posting, start_date, skill_options, assign_to_options, skill_default, assign_to_default } = this.state
    return (
      <Fragment>
        {/* <TitleComponent title="Login"/> */}
        <Container className="my-5">
          <Row>
            <Col className="col-12 col-lg-6 m-auto">
              <Form onSubmit={this.onSubmit}>
                <Card>
                  <CardBody>
                    <Row>
                      <Col>
                        <h1 className="text-center">Edit Task</h1>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="title">Title</Label>
                          <Input
                            type="text"
                            name="title"
                            id="title"
                            value={title}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("title", title)}
                            maxLength={25}
                          />
                          <FormFeedback>Enter valid Title</FormFeedback>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Label for="due_date">Date of birth</Label>
                          <DatePicker
                            selected={due_date}
                            minDate={start_date}
                            onChange={this.handleDateChange}
                            invalid={Validation.init("date", due_date)}
                            className="form-control"
                            dateFormat="yyyy-MM-dd"
                          />
                          <FormFeedback>Enter valid Date of birth</FormFeedback>
                        </FormGroup>
                      </Col>
                      </Row>
                      <Row>
                      <Col>
                        <FormGroup>
                          <Label for="description">Description</Label>
                          <Input
                            type="textarea"
                            name="description"
                            id="description"
                            value={description}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("description", description)}
                          />
                          <FormFeedback>Enter valid Description</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="skills">Skills</Label>
                          <Select
                            defaultValue={skill_default}
                            options={skill_options}
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            onChange={this.handleSkillChange}
                            isMulti
                            invalid={Validation.init("skills", skills)}
                          />
                            <FormFeedback>Select at least one skill</FormFeedback>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Label for="assign_to">Assign To</Label>
                          <Select
                            defaultValue={assign_to_default}
                            options={assign_to_options}
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            onChange={this.handleAssignToChange}
                            isMulti
                            invalid={Validation.init("assign_to", assign_to)}
                          />
                            <FormFeedback>Select at least Assign</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Button disabled={!formValid || posting} type="submit">
                          Submit
                        </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Form>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}
export default EditTask;
