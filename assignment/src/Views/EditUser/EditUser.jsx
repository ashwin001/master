import React, { Component, Fragment } from "react";
import { Container, Card, CardBody, Row, Col, Form, FormGroup, Label, Input, FormFeedback, Button } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import { notify } from 'react-notify-toast';
import { Validation, Http } from "../../Helpers";
import { API, CONSTANT } from "../../Constants";
import _ from "lodash";
const animatedComponents = makeAnimated();
class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      formValid: false,
      first_name: this.props.location.state.userData.name.first_name,
      last_name: this.props.location.state.userData.name.last_name,
      email_address: this.props.location.state.userData.email_address,
      password: "",
      confirm_password: "",
      dob: new Date(this.props.location.state.userData.dob),
      skills: this.props.location.state.userData.skills,
      account_type: Number(this.props.location.state.userData.account_type),
      start_date: new Date,
      skill_options: [],
      account_type_options: [],
      skill_default: [],
    }
    _.map(CONSTANT.SKILLS, (skill, index) => {
      this.state.skill_options.push({value: index, label: skill})
    })
    _.map(CONSTANT.ACCOUNT_TYPE, (account, index) => {
        this.state.account_type_options.push({value: index, label: account})
    })
    _.map(this.props.location.state.userData.skills, (skill) => {
      this.state.skill_options.find(s => {
        if (Number(skill) === Number(s.value))
          this.state.skill_default.push(s);
      })
    })
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSkillChange = this.handleSkillChange.bind(this);
    this.handleAccountTypeChange = this.handleAccountTypeChange.bind(this);
  }

  handleAccountTypeChange = (account_type) => {
    this.setState({ account_type: account_type.value }, () => { this.validateForm() });
  }

  handleSkillChange = (skills) => {
    let skillArray = [];
    _.map(skills, (skill) => {
      skillArray.push(skill.value)
    })
    this.setState({ skills: skillArray }, () => { this.validateForm() });
  }

  handleDateChange (date) {
    this.setState({dob: date});
  }

  handleChange = (e) => {
    const name = e.target.name;
    let value = e.target.value;
    if ((name === "password") || (name === "confirm_password"))
      value = value.trim();

    this.setState({ [name]: value }, this.validateForm);
  };

  validateForm() {
    const { first_name, last_name, email_address, password, confirm_password, dob, skills, account_type } = this.state;
    this.setState({
      formValid:
        first_name !== "" &&
        last_name !== "" &&
        email_address !== "" &&
        password !== "" &&
        confirm_password !== "" &&
        dob !== "" &&
        skills.length > 0 &&
        account_type !== "" &&
        (Validation.init("required", first_name) === false) &&
        (Validation.init("required", last_name) === false) &&
        (Validation.init("required", email_address) === false) &&
        (Validation.init("required", password) === false) &&
        (Validation.init("required", confirm_password) === false) &&
        (Validation.init("first_name", first_name) === false) &&
        (Validation.init("last_name", last_name) === false) &&
        (Validation.init("email_address", email_address) === false) &&
        (Validation.init("password", password) === false) &&
        (Validation.init("confirm_password", confirm_password) === false) &&
        (Validation.init("date", dob) === false)
    });
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { first_name, last_name, email_address, password, confirm_password, dob, skills, account_type } = this.state;
    let param = { first_name, last_name, email_address, password, confirm_password, dob, skills, account_type }
    this.setState({ posting: true });
    Http.post(API.ADD_USER, param)
      .then((response) => {
        this.setState({ posting: false });
        notify.show(response.message, "success", 5000, { background: '#0E1717', text: "#FFFFFF" });
        this.props.history.push("/users");
      })
      .catch(() => {
        this.setState({ posting: false });
      });
  }

  render() {
    const { first_name, last_name, email_address, password, confirm_password, dob, skills, account_type, formValid, posting, start_date, skill_options, account_type_options, skill_default } = this.state
    return (
      <Fragment>
        {/* <TitleComponent title="Login"/> */}
        <Container className="my-5">
          <Row>
            <Col className="col-12 col-lg-6 m-auto">
              <Form onSubmit={this.onSubmit}>
                <Card>
                  <CardBody>
                    <Row>
                      <Col>
                        <h1 className="text-center">Edit User</h1>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="first_name">First Name</Label>
                          <Input
                            type="text"
                            name="first_name"
                            id="first_name"
                            value={first_name}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("first_name", first_name)}
                            maxLength={25}
                          />
                          <FormFeedback>Enter valid First Name</FormFeedback>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Label for="last_name">Last Name</Label>
                          <Input
                            type="text"
                            name="last_name"
                            id="last_name"
                            value={last_name}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("last_name", last_name)}
                            maxLength={25}
                          />
                          <FormFeedback>Enter valid Last Name</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="email_address">Email Address</Label>
                          <Input
                            type="text"
                            name="email_address"
                            id="email_address"
                            value={email_address}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("email_address", email_address)}
                            maxLength={150}
                          />
                          <FormFeedback>Enter valid Email Address</FormFeedback>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Label for="dob">Date of birth</Label>
                          <DatePicker
                            selected={dob}
                            maxDate={start_date}
                            onChange={this.handleDateChange}
                            invalid={Validation.init("date", dob)}
                            className="form-control"
                          />
                          <FormFeedback>Enter valid Date of birth</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="password">Password</Label>
                          <Input
                            type="password"
                            name="password"
                            id="password"
                            value={password}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("password", password)}
                            maxLength={25}
                          />
                          <FormFeedback>Enter valid Password</FormFeedback>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Label for="password">Confirm Password</Label>
                          <Input
                            type="password"
                            name="confirm_password"
                            id="confirm_password"
                            value={confirm_password}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("confirm_password", confirm_password)}
                            maxLength={25}
                          />
                          <FormFeedback>Enter valid Confirm Password</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="skills">Skills</Label>
                          <Select
                            defaultValue={skill_default}
                            options={skill_options}
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            onChange={this.handleSkillChange}
                            isMulti
                            invalid={Validation.init("skills", skills)}
                            />
                            <FormFeedback>Select at least one skill</FormFeedback>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Label for="account_type">Account Type</Label>
                          <Select
                            value={account_type_options.find(option => {
                              return option.value == account_type
                            })}
                            options={account_type_options}
                            onChange={this.handleAccountTypeChange}
                            invalid={Validation.init("account_type", account_type)}
                            />
                            <FormFeedback>Select at least Assign</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Button disabled={!formValid || posting} type="submit">
                          Submit
                        </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Form>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}
export default EditUser;
