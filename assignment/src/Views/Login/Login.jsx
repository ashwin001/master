import React, { Component, Fragment } from "react";
import { Container, Card, CardBody, Row, Col, Form, FormGroup, Label, Input, FormFeedback, Button } from "reactstrap";
//import { TitleComponent } from "../../Components";
import { Validation, AuthHelper } from "../../Helpers";
import { Auth } from "../../Services";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      formValid: false,
      email_address : "ashwin@mailinator.com",
      password : "Admin420!",
    }
  }

  handleChange = (e) => {
    const name = e.target.name;
    let value = e.target.value;
    if (name === "password")
      value = value.trim();

    this.setState({ [name]: value }, this.validateForm);
  };

  validateForm() {
    const { email_address, password } = this.state;
    this.setState({
      formValid:
        email_address !== "" &&
        password !== "" &&
        Validation.isFormValid(["email_address", "password"], this.state)
    });
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ posting: true });
    const { email_address, password } = this.state;
    Auth.login(email_address, password)
      .then(
        data => {
          if (data.STATUS !== "FAILURE")
          {
            this.props.history.push("/dashboard");
            setTimeout(() => {
              window.location.reload(false);
            }, 10);
          }
        },
        error => {
          console.log(error)
        }
      );
  }

  componentDidMount(){
    if (AuthHelper.getAuth())
      this.props.history.push("/dashboard");
  }

  render() {
    const { email_address, password, formValid, posting } = this.state;
    return (
      <Fragment>
        {/* <TitleComponent title="Login"/> */}
        <Container>
          <Row className="my-5">
            <Col className="col-12 col-lg-6 m-auto">
              <Form onSubmit={this.onSubmit}>
                <Card>
                  <CardBody>
                    <Row>
                      <Col>
                        <h1 className="text-center">Login</h1>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="email_address">Email Address</Label>
                          <Input
                            type="text"
                            name="email_address"
                            id="email_address"
                            value={email_address}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("email_address", email_address)}
                            maxLength={150}
                          />
                          <FormFeedback>Enter valid Email Address</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label for="password">Password</Label>
                          <Input
                            type="password"
                            name="password"
                            id="password"
                            value={password}
                            autoComplete="off"
                            onChange={this.handleChange}
                            invalid={Validation.init("password", password)}
                            maxLength={25}
                          />
                          <FormFeedback>Enter valid Password</FormFeedback>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Button className="btn btn-success" disabled={!formValid || posting} type="submit">
                          Login
                        </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Form>
            </Col>
          </Row>
        </Container>
      </Fragment>
    )
  }
}
export default Login;
