import React, { Component, Fragment } from "react";
import { Container, Row, Col, Table, Button } from "reactstrap";
import _ from "lodash";
import { Link } from "react-router-dom";
import { API, CONSTANT } from "../../Constants";
import { Http, Utils } from "../../Helpers";
import { Auth } from "../../Services";
import { ConfirmationModal } from "../../Modals";
class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      tasks: [],
      users: [],
      confirmationModalShow: false,
      confirmMessage: "",
      confirmTitle: "",
      confirmPopupEvent: "",
      selectedTask: "",
    }
  }

  getUserList = () => {
    this.setState({ posting: true });

    Http.get(API.GET_ALL_USER_LIST)
      .then((response) => {
        this.setState({
          users: response.DATA,
          posting: false
        });
      })
      .catch((error) => {
        this.setState({ users: [], posting: false });
      });
  };

  deleteTask = () => {
    const { selectedTask } = this.state;
    this.setState({ posting: true });

    Http.delete_request(API.DELETE_TASK+"/"+selectedTask._id)
      .then((response) => {
        this.setState({
          posting: false,
        }, ()=>{
          this.getTaskList();
        });
      })
      .catch((error) => {
        this.setState({ tasks: [], posting: false });
      });
  };

  getTaskList = () => {
    this.setState({ posting: true });

    Http.get(API.GET_ALL_TASK_LIST)
      .then((response) => {
        this.setState({
          tasks: response.DATA,
          posting: false,
        });
      })
      .catch((error) => {
        this.setState({ tasks: [], posting: false });
      });
  };

  editTaskAccount = (task) => {
    this.props.history.push({
      pathname: "/edit_task",
      state: { taskData: task },
    });
  }

  componentDidMount() {
    this.getUserList();
    this.getTaskList();
  }
  
  modalShow = (item) => {
    this.setState({
      selectedTask: item,
      confirmationModalShow: true,
    });
  };

  modalHide = () => {
    this.setState({
      confirmationModalShow: false,
    })
  }
  callback = () => {
    this.modalHide();
    this.deleteTask();
  }

  render() {
    const { tasks, users, confirmationModalShow, selectedTask } = this.state;
    const ConfirmationModalProps = {
      itemProps: selectedTask,
      isOpen: confirmationModalShow,
      toggle: this.modalHide,
      message: this.state.confirmMessage,
      title: this.state.confirmTitle,
      handleChange: this.handleChange,
      callback: () => this.callback(),
    };
    return (
      <Fragment>
        <Container className="my-5 container-fluid">
          <Row>
            <Col><h1>Tasks</h1></Col>
            <Col className="my-auto">
              <Link to="/add_task" className="float-right btn btn-success">Add Task</Link>
            </Col>
          </Row>
          <Row>
            <Col className="table-responsive">
              <Table className="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Skill set</th>
                    <th>Due Date</th>
                    <th>Assigned Manager</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                {
                  tasks.length > 0 ?
                  (
                    _.map(tasks, (task, index) => {
                      return (
                        <tr key={index}>
                          <td style={{maxWidth: "200px"}}><span className={Utils.DueDateGraterThanCurrentDate(task.due_date) > 0 ? "text-danger" : ""}>{task.title}</span></td>
                          <td style={{maxWidth: "200px"}}>{task.description}</td>
                          <td style={{maxWidth: "200px"}}>
                            {
                              _.map(CONSTANT.SKILLS, (skill, index) => {
                                return (
                                  task.skills.indexOf(index) > -1 &&
                                    skill+", "
                                )
                              })
                            }
                          </td>
                          <td>{Utils.DobFormat(task.due_date)}</td>
                          <td style={{maxWidth: "200px"}}>
                            {
                              users.length > 0 &&
                              _.map(users, (user) => {
                                return (
                                  task.assign_to.indexOf(user._id) > -1 &&
                                    user.name.first_name+" "+user.name.last_name+", "
                                )
                              })
                            }
                          </td>
                          <td><span className={task.status ? "text-success" : "text-danger"}>{task.status ? "Active" : "Inactive"}</span></td>
                          <td>
                            {
                              Number(Auth.loggedUserValue.account_type) === 1 &&
                              <Fragment>
                                <Button className="btn btn-success btn-sm" onClick={()=>this.editTaskAccount(task)}>Edit</Button>
                                <Button className="ml-2 btn btn-danger btn-sm"
                                  onClick={() => {
                                    this.setState({
                                      confirmTitle: "Delete "+task.title+" Account",
                                      confirmMessage: "Are you sure you want to delete "+task.title+" account?"
                                    }, () => { 
                                      this.modalShow(task)
                                    }) 
                                  }}
                                >Delete</Button>
                              </Fragment>
                            }
                          </td>
                        </tr>
                      )
                    })
                  ) : (
                    <tr>
                      <td colSpan="7">No record found</td>
                    </tr>
                  )
                }
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
        {
          confirmationModalShow && (
            <ConfirmationModal {...ConfirmationModalProps} />
          )
        }
      </Fragment>
    )
  }
}
export default Tasks;
