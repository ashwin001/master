import React, { Component, Fragment } from "react";
import { Container, Row, Col, Table, Button } from "reactstrap";
import _ from "lodash";
import { Link } from "react-router-dom";
import { API, CONSTANT } from "../../Constants";
import { Http, Utils } from "../../Helpers";
import { Auth } from "../../Services";
import { ConfirmationModal } from "../../Modals";
class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posting: false,
      users: [],
      confirmationModalShow: false,
      confirmMessage: "",
      confirmTitle: "",
      confirmPopupEvent: "",
      selectedUser: "",
    }
  }

  getUserList = () => {
    this.setState({ posting: true });

    Http.get(API.GET_ALL_USER_LIST)
      .then((response) => {
        this.setState({
          users: response.DATA,
          posting: false,
        });
      })
      .catch((error) => {
        this.setState({ users: [], posting: false });
      });
  };

  deleteUser = () => {
    const { selectedUser } = this.state;
    this.setState({ posting: true });

    Http.delete_request(API.DELETE_USER+"/"+selectedUser._id)
      .then((response) => {
        this.setState({
          posting: false,
        }, ()=>{
          this.getUserList();
        });
      })
      .catch((error) => {
        this.setState({ users: [], posting: false });
      });
  };

  editUserAccount = (user) => {
    this.props.history.push({
      pathname: "/edit_user",
      state: { userData: user },
    });
  }
  
  componentDidMount() {
    this.getUserList();
  }
  
  modalShow = (item) => {
    this.setState({
      selectedUser: item,
      confirmationModalShow: true,
    });
  };

  modalHide = () => {
    this.setState({
      confirmationModalShow: false,
    })
  }
  callback = () => {
    this.modalHide();
    this.deleteUser();
  }

  render() {
    const { users, confirmationModalShow, selectedUser } = this.state;
    const ConfirmationModalProps = {
      itemProps: selectedUser,
      isOpen: confirmationModalShow,
      toggle: this.modalHide,
      message: this.state.confirmMessage,
      title: this.state.confirmTitle,
      handleChange: this.handleChange,
      callback: () => this.callback(),
    };
    return (
      <Fragment>
        <Container className="my-5 container-fluid">
          <Row>
            <Col><h1>Users</h1></Col>
            <Col className="my-auto">
              <Link to="/add_user" className="float-right btn btn-success">Add User</Link>
            </Col>
          </Row>
          <Row>
            <Col className="table-responsive">
              <Table className="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Account Type</th>
                    <th>Date of birth</th>
                    <th>Age</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                {
                  users.length > 0 ?
                  (
                    _.map(users, (user, index) => {
                      return (
                        <tr key={index}>
                          <td>{user.name.first_name+" "+user.name.last_name}</td>
                          <td>{user.email_address}</td>
                          <td>{CONSTANT.ACCOUNT_TYPE[user.account_type]}</td>
                          <td>{Utils.DobFormat(user.dob)}</td>
                          <td>{Utils.CalculateAge(user.dob)+" Year's"}</td>
                          <td><span className={user.status ? "text-success" : "text-danger"}>{user.status ? "Active" : "Inactive"}</span></td>
                          <td>
                            {
                              Number(user.account_type) >= Number(Auth.loggedUserValue.account_type) &&
                              <Fragment>
                                <Button className="btn btn-success btn-sm" onClick={()=>this.editUserAccount(user)}>Edit</Button>
                                {
                                  Number(Auth.loggedUserValue.account_type) !== Number(user.account_type) &&
                                  <Button className="ml-2 btn btn-danger btn-sm"
                                    onClick={() => {
                                      this.setState({
                                        confirmTitle: "Delete "+user.name.first_name+" "+user.name.last_name+" Account",
                                        confirmMessage: "Are you sure you want to delete "+user.name.first_name+" "+user.name.last_name+" account?"
                                      }, () => { 
                                        this.modalShow(user)
                                      }) 
                                    }}
                                  >Delete</Button>
                                }
                              </Fragment>
                            }
                          </td>
                        </tr>
                      )
                    })
                  ) : (
                    <tr>
                      <td colSpan="7">No record found</td>
                    </tr>
                  )
                }
                </tbody>
              </Table>
            </Col>
          </Row>
        </Container>
        {
          confirmationModalShow && (
            <ConfirmationModal {...ConfirmationModalProps} />
          )
        }
      </Fragment>
    )
  }
}
export default Users;
