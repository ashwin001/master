import _ from 'lodash';

const DevHostname = [
    'localhost',
]

const StagingHostname = [
    'assignmentstag.com'
]

const TestingHostname = [
    'assignmenttest.com'
]

const hostname = window.location.hostname;
const protocol = window.location.protocol;

const development = {
    apiURL: {
        URL: protocol + "//localhost:3006",
    },
    cognito: {
        WEB_SITE_NAME: "Assignment"
    }
};

const staging = {
    apiURL: {
        URL: protocol + "//localhost:3006",
    },
    cognito: {
        WEB_SITE_NAME: "Assignment Staging"
    }
};

const testing = {
    apiURL: {
        URL: protocol + "//localhost:3006",
    },
    cognito: {
        WEB_SITE_NAME: "Assignment Testing"
    }
};

const production = {
    apiURL: {
        URL: protocol + "//localhost:3006",
    },
    cognito: {
        WEB_SITE_NAME: "Assignment Production"
    }
};

const config = _.includes(DevHostname, hostname) ? development : (
    (_.includes(StagingHostname, hostname) ? staging : (
    _.includes(TestingHostname, hostname) ? testing : production)));

export default {
    ...config
};