import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as dotenv from "dotenv";
import * as cors from "cors";
import { CommonRoutes } from "./routes/commonRoutes";
import { UserRoutes } from "./routes/userRoutes";
import { TaskRoutes } from "./routes/taskRoutes";

dotenv.config({path: __dirname +'/../.env'});
class App {

    public app: express.Application;

    public mongoUrl: string = 'mongodb://localhost/' + process.env.DB_DATABASE;
    //public mongoUrl: string = 'mongodb://dalenguyen:123123@localhost:27017/CRMdb';Server URL

    private common_routes: CommonRoutes = new CommonRoutes();
    private user_routes: UserRoutes = new UserRoutes();
    private task_routes: TaskRoutes = new TaskRoutes();
    
    constructor()
    {
        this.app = express();
        this.config();
        this.mongoSetup();
        this.common_routes.routes(this.app);
        this.user_routes.routes(this.app);
        this.task_routes.routes(this.app);
    }

    private config(): void
    {
        //Allow cros 
        this.app.use(cors());
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
        // serving static files 
        this.app.use(express.static('public'));
    }

    //MongoDB connection start by Ashwin Bhayal 21/04/2021
    private mongoSetup(): void
    {
        //mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
    }
    //MongoDB connection start by Ashwin Bhayal 21/04/2021
}

export default new App().app;