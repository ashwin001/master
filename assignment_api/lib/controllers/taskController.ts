import { Request, Response } from 'express';
import { successResponse, failureResponse } from '../modules/common/service';
import { ValidationHelper } from '../helper/validationHelper';
import Logger from "../logging/logger";
import { ITask } from '../modules/task/model';
import TaskService from '../modules/task/service';

export class TaskController
{
    private logger: Logger = new Logger();
    private task_service: TaskService = new TaskService();
    private validation_helper: ValidationHelper = new ValidationHelper();

    //Create a new Task start by Ashwin Bhayal 21/04/2021
    public create_task = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.body.title))
                throw "Title is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.title, 3))
                throw "Title is allow to minimum 3 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.title, 25))
                throw "Title is allow to maximum 25 alphabets!";
            else if (!this.validation_helper.inputRequired(req.body.description))
                throw "Last Name is required!";
            else if (!this.validation_helper.inputValidateDate(req.body.due_date))
                throw "Invalidate due date!";
            else if (!this.validation_helper.inputEmptyArray(req.body.skills))
                throw "Skill is required!";
            else if (!this.validation_helper.inputEmptyArray(req.body.assign_to))
                throw "Account Type is required!";
            
            //Create a new Task start by Ashwin Bhayal 21/04/2021
            const task_params: ITask = {
                title: req.body.title,
                description: req.body.description,
                due_date: req.body.due_date,
                skills: req.body.skills,
                assign_to: req.body.assign_to,
                modification_notes: [{
                    modified_on: new Date(Date.now()),
                    modified_by: null,
                    modification_note: 'New Task created'
                }]
            };
            this.task_service.createTask(task_params, (err: any, task_data: ITask) => {
                //Getting MongoDB error start by Ashwin Bhayal 21/04/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in create_task() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error!", req.body, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 21/04/2021
                //Created a new Task start by Ashwin Bhayal 21/04/2021
                else
                    successResponse('Create Task successfull', task_data, res);
                //Created a new Task end by Ashwin Bhayal 21/04/2021
            });
            //Create a new Task start by Ashwin Bhayal 21/04/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in create_task() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.body, res)
        }
    }
    //Create a new Task end by Ashwin Bhayal 21/04/2021

    //Get All Task start by Ashwin Bhayal 21/04/2021
    public get_tasks = async (req: Request, res: Response) =>
    {
        try
        {
            //Get All Task start by Ashwin Bhayal 21/04/2021
            const task_filter = {};
            this.task_service.getAllTask(task_filter, (err: any, task_data: ITask) => {
                //Getting MongoDB error start by Ashwin Bhayal 21/04/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_tasks() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error!", req.body, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 21/04/2021
                //Get All Task start by Ashwin Bhayal 21/04/2021
                else
                    successResponse('Get Task successfull', task_data, res);
                //Get All Task end by Ashwin Bhayal 21/04/2021
            });
            //Get All Task end by Ashwin Bhayal 21/04/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_tasks() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, null, res)
        }
    }
    //Get All Task end by Ashwin Bhayal 21/04/2021

    //Get Task by id start by Ashwin Bhayal 21/04/2021
    public get_task = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.params.id))
                throw "Task id is required!";
            
            //Get Task data by id start by Ashwin Bhayal 21/04/2021
            const task_filter = { _id: req.params.id };
            this.task_service.filterTask(task_filter, (err: any, task_data: ITask) => {
                //Getting MongoDB error start by Ashwin Bhayal 21/04/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_task() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error!", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 21/04/2021
                //Get Task data by id start by Ashwin Bhayal 21/04/2021
                else if (task_data)
                    successResponse('Get Task successfull', task_data, res);
                //Get Task data by id end by Ashwin Bhayal 21/04/2021
                //Task id does not exist start by Ashwin Bhayal 21/04/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_task() function : ' + req.params.id +" Task id does not exist"});
                    // error response if some fields are missing in request body
                    failureResponse("Task id does not exist!", req.params.id, res)
                }
                //Task id does not exist end by Ashwin Bhayal 21/04/2021
            });
            //Get Task data by id end by Ashwin Bhayal 21/04/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_task() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.params.id, res)
        }
    }
    //Get Task by id end by Ashwin Bhayal 21/04/2021

    //Update Task by id start by Ashwin Bhayal 21/04/2021
    public update_task = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.params.id))
                throw "Client id is required!";
            else if (!this.validation_helper.inputRequired(req.body.title))
                throw "Title is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.title, 3))
                throw "Title is allow to minimum 3 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.title, 25))
                throw "Title is allow to maximum 25 alphabets!";
            else if (!this.validation_helper.inputRequired(req.body.description))
                throw "Last Name is required!";
            else if (!this.validation_helper.inputValidateDate(req.body.due_date))
                throw "Invalidate due date!";
            else if (!this.validation_helper.inputEmptyArray(req.body.skills))
                throw "Skill is required!";
            else if (!this.validation_helper.inputEmptyArray(req.body.assign_to))
                throw "Account Type is required!";
            
            
            //Get Task data by id start by Ashwin Bhayal 21/04/2021
            const task_filter = { _id: req.params.id };
            this.task_service.filterTask(task_filter, (err: any, task_data: ITask) => {
                //Getting MongoDB error start by Ashwin Bhayal 21/04/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_task() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error!", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 21/04/2021
                //Task id is exit start by Ashwin Bhayal 21/04/2021
                else if (task_data)
                {
                    //Update Task data by id start Ashwin Bhayal 21/04/2021
                    task_data.modification_notes.push({
                        modified_on: new Date(Date.now()),
                        modified_by: null,
                        modification_note: 'Task data updated'
                    });
                    const task_params: ITask = {
                        _id: req.params.id,
                        title: req.body.title ? req.body.title : task_data.title,
                        description: req.body.description ? req.body.description : task_data.description,
                        due_date: req.body.due_date ? req.body.due_date : task_data.due_date,
                        skills: req.body.skills ? req.body.skills : task_data.skills,
                        assign_to: req.body.assign_to ? req.body.assign_to : task_data.assign_to,
                        status: req.body.status ? req.body.status : task_data.status,
                        is_deleted: req.body.is_deleted ? req.body.is_deleted : task_data.is_deleted,
                        modification_notes: task_data.modification_notes
                    };
                    this.task_service.updateTask(task_params, (err: any) => {
                        //Getting MongoDB error start by Ashwin Bhayal 21/04/2021
                        if (err)
                        {
                            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_task() function : ' + err});
                            // error response if some fields are missing in request body
                            failureResponse("Getting MongoDB error!", req.params.id, res)
                        }
                        //Getting MongoDB error end by Ashwin Bhayal 21/04/2021
                        //Update Task data by id start Ashwin Bhayal 21/04/2021
                        else
                            successResponse('Update Task successfull', null, res);
                        //Update Task data by id end Ashwin Bhayal 21/04/2021
                    });
                    //Update Task data by id end Ashwin Bhayal 21/04/2021
                }
                //Task id is sexist end by Ashwin Bhayal 21/04/2021
                //Task id does not exist start by Ashwin Bhayal 21/04/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_task() function : ' + req.params.id +" Task id does not exist"});
                    // error response if some fields are missing in request body
                    failureResponse("Task id does not exist!", req.params.id, res)
                }
                //Task id does not exist end by Ashwin Bhayal 21/04/2021
            });
            //Get Task data by id end by Ashwin Bhayal 21/04/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_task() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.body, res)
        }
    }
    //Update Task by id end by Ashwin Bhayal 21/04/2021

    //Delete Task by id start by Ashwin Bhayal 21/04/2021
    public delete_task = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.params.id))
                throw "Task id is required!";
            
            //Delete Task by id start by Ashwin Bhayal 21/04/2021
            this.task_service.deleteTask(req.params.id, (err: any, delete_details) => {
                //Getting MongoDB error start by Ashwin Bhayal 21/04/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in delete_task() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error!", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 21/04/2021
                //Deleted Task data by id start by Ashwin Bhayal 21/04/2021
                else if (delete_details.deletedCount !== 0)
                    successResponse('Delete Task successfull', null, res);
                //Deleted Task data by id end by Ashwin Bhayal 21/04/2021
                //Task id does not exist start by Ashwin Bhayal 21/04/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in delete_task() function : ' + req.params.id +" Task id does not exist"});
                    // error response if some fields are missing in request body
                    failureResponse("Task id does not exist!", req.params.id, res)
                }
                //Task id does not exist end by Ashwin Bhayal 21/04/2021
            });
            //Delete Task by id end by Ashwin Bhayal 21/04/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in delete_task() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.params.id, res)
        }
    }
    //Delete Task by id end by Ashwin Bhayal 21/04/2021
}