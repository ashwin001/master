import { Request, Response } from 'express';
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import * as dotenv from "dotenv";
import { successResponse, failureResponse } from '../modules/common/service';
import { ValidationHelper } from '../helper/validationHelper';
import Logger from "../logging/logger";
import { IUser, IUserGet } from '../modules/user/model';
import UserService from '../modules/user/service';

dotenv.config({path: __dirname +'/../.env'});

export class UserController
{
    private logger: Logger = new Logger();
    private user_service: UserService = new UserService();
    private validation_helper: ValidationHelper = new ValidationHelper();

    //Create a new User start by Ashwin Bhayal 24/03/2021
    public create_user = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.body.first_name))
                throw "First Name is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.first_name, 3))
                throw "First Name is allow to minimum 3 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.first_name, 25))
                throw "First Name is allow to maximum 25 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyAlphabets(req.body.first_name))
                throw "First Name is allow only alphabets!";
            else if (!this.validation_helper.inputRequired(req.body.last_name))
                throw "Last Name is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.last_name, 3))
                throw "Last Name is allow to minimum 3 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.last_name, 25))
                throw "Last Name is allow to maximum 25 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyAlphabets(req.body.last_name))
                throw "Last Name is allow only alphabets!";
            else if (!this.validation_helper.inputRequired(req.body.email_address))
                throw "Email Addess is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.email_address, 10))
                throw "Email Addess is allow to minimum 10 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.email_address, 150))
                throw "Email Addess is allow to maximum 150 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyEmail(req.body.email_address))
                throw "Invalid Email Addess!";
            else if (!this.validation_helper.inputRequired(req.body.password))
                throw "Password is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.password, 8))
                throw "Password is allow to minimum 8 characters!";
            else if (!this.validation_helper.inputMaximumLength(req.body.password, 25))
                throw "Password is allow to maximum 25 characters!";
            else if (!this.validation_helper.inputValidateDate(req.body.dob))
                throw "Invalidate date of birth!";
            else if (!this.validation_helper.inputAllowOnlyNumbers(req.body.account_type))
                throw "Account Type is allow only number!";
            else if (!this.validation_helper.inputEmptyArray(req.body.skills))
                throw "Skill is required!";

            //Password convert to hesh
			req.body.password = await new Promise((resolve, reject) => {
				bcrypt.hash(req.body.password, 10, function (err, hash) {
					if (err) reject(err);
					resolve(hash);
				});
			});
            //Get User data by id start by Ashwin Bhayal 24/03/2021
            const user_filter = { email_address: req.body.email_address };
            this.user_service.filterUser(user_filter, (err: any, user_data: IUser) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in create_user() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error!", req.body, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //Get User data by id start by Ashwin Bhayal 24/03/2021
                else if (user_data)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in create_user() function : Email address already exist' + user_data.email_address});
                    // error response if some fields are missing in request body
                    failureResponse("Email address already exist!", req.body, res)
                }
                //Get User data by id end by Ashwin Bhayal 24/03/2021
                else
                {
                    //Create a new User start by Ashwin Bhayal 24/03/2021
                    const user_params: IUser = {
                        name: {
                            first_name: req.body.first_name,
                            last_name: req.body.last_name
                        },
                        email_address: req.body.email_address,
                        password: req.body.password,
                        dob: req.body.dob,
                        skills: req.body.skills,
                        account_type: Number(req.body.account_type),
                        modification_notes: [{
                            modified_on: new Date(Date.now()),
                            modified_by: null,
                            modification_note: 'New User created'
                        }]
                    };
                    this.user_service.createUser(user_params, (err: any, user_data: IUser) => {
                        //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                        if (err)
                        {
                            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in create_user() function : ' + err});
                            // error response if some fields are missing in request body
                            failureResponse("Getting MongoDB error!", req.body, res)
                        }
                        //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                        //Created a new User start by Ashwin Bhayal 24/03/2021
                        else
                            successResponse('Create User successfull', user_data, res);
                        //Created a new User end by Ashwin Bhayal 24/03/2021
                    });
                    //Create a new User start by Ashwin Bhayal 24/03/2021
                }
            });
            //Get User data by id end by Ashwin Bhayal 24/03/2021            
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in create_user() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.body, res)
        }
    }
    //Create a new User end by Ashwin Bhayal 24/03/2021

    //Get All User start by Ashwin Bhayal 24/03/2021
    public get_users = async (req: Request, res: Response) =>
    {
        try
        {
            //Get All User start by Ashwin Bhayal 24/03/2021
            const user_filter = {};
            this.user_service.getAllUser(user_filter, (err: any, user_data: IUserGet) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_users() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.body, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //Get All User start by Ashwin Bhayal 24/03/2021
                else
                    successResponse('Get User successfull', user_data, res);
                //Get All User end by Ashwin Bhayal 24/03/2021
            });
            //Get All User end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_users() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, null, res)
        }
    }
    //Get All User end by Ashwin Bhayal 24/03/2021

    

    //Get All User start by Ashwin Bhayal 24/03/2021
    public get_users_account_type = async (req: Request, res: Response) =>
    {
        try
        {
            //Get All User start by Ashwin Bhayal 24/03/2021
            const user_filter = {account_type: req.body.account_type};
            this.user_service.getAllUser(user_filter, (err: any, user_data: IUserGet) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_users() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.body, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //Get All User start by Ashwin Bhayal 24/03/2021
                else
                    successResponse('Get User successfull', user_data, res);
                //Get All User end by Ashwin Bhayal 24/03/2021
            });
            //Get All User end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_users() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, null, res)
        }
    }
    //Get All User end by Ashwin Bhayal 24/03/2021

    //Get User by id start by Ashwin Bhayal 24/03/2021
    public get_user = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.params.id))
                throw "User id is required!";
            
            //Get User data by id start by Ashwin Bhayal 24/03/2021
            const user_filter = { _id: req.params.id };
            this.user_service.filterUser(user_filter, (err: any, user_data: IUserGet) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_user() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //Get User data by id start by Ashwin Bhayal 24/03/2021
                else if (user_data)
                    successResponse('Get User successfull', user_data, res);
                //Get User data by id end by Ashwin Bhayal 24/03/2021
                //User id does not exist start by Ashwin Bhayal 24/03/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_user() function : ' + req.params.id + " User id does not exist!"});
                    // error response if some fields are missing in request body
                    failureResponse("User id does not exist!", req.params.id, res)
                }
                //User id does not exist end by Ashwin Bhayal 24/03/2021
            });
            //Get User data by id end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in get_user() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.params.id, res)
        }
    }
    //Get User by id end by Ashwin Bhayal 24/03/2021

    //Update User by id start by Ashwin Bhayal 24/03/2021
    public update_user = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.params.id))
                throw "User id is required!";
            else if (!this.validation_helper.inputRequired(req.body.first_name))
                throw "First Name is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.first_name, 3))
                throw "First Name is allow to minimum 3 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.first_name, 25))
                throw "First Name is allow to maximum 25 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyAlphabets(req.body.first_name))
                throw "First Name is allow only alphabets!";
            else if (!this.validation_helper.inputRequired(req.body.last_name))
                throw "Last Name is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.last_name, 3))
                throw "Last Name is allow to minimum 3 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.last_name, 25))
                throw "Last Name is allow to maximum 25 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyAlphabets(req.body.last_name))
                throw "Last Name is allow only alphabets!";
            else if (!this.validation_helper.inputRequired(req.body.email_address))
                throw "Email Addess is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.email_address, 10))
                throw "Email Addess is allow to minimum 10 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.email_address, 150))
                throw "Email Addess is allow to maximum 150 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyEmail(req.body.email_address))
                throw "Invalid Email Addess!";
            else if (!this.validation_helper.inputRequired(req.body.password))
                throw "Password is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.password, 8))
                throw "Password is allow to minimum 8 characters!";
            else if (!this.validation_helper.inputMaximumLength(req.body.password, 25))
                throw "Password is allow to maximum 25 characters!";
            else if (!this.validation_helper.inputValidateDate(req.body.dob))
                throw "Invalidate date of birth!";
            else if (!this.validation_helper.inputAllowOnlyNumbers(req.body.account_type))
                throw "Account Type is required!";
            else if (!this.validation_helper.inputEmptyArray(req.body.skill))
                throw "Skill is required!";
            
            
            //Get User data by id start by Ashwin Bhayal 24/03/2021
            const user_filter = { _id: req.params.id };
            this.user_service.filterUser(user_filter, (err: any, user_data: IUser) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_user() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //User id is exit start by Ashwin Bhayal 24/03/2021
                else if (user_data)
                {
                    //Update User data by id start Ashwin Bhayal 24/03/2021
                    user_data.modification_notes.push({
                        modified_on: new Date(Date.now()),
                        modified_by: null,
                        modification_note: 'User data updated'
                    });
                    const user_params: IUser = {
                        _id: req.params.id,
                        name: {
                            first_name: req.body.first_name ? req.body.first_name : user_data.name.first_name,
                            last_name: req.body.last_name ? req.body.last_name : user_data.name.last_name
                        },
                        email_address: req.body.email_address ? req.body.email_address : user_data.email_address,
                        password: req.body.password ? req.body.password : user_data.password,
                        dob: req.body.dob ? req.body.dob : user_data.dob,
                        skills: req.body.skills ? req.body.skills : user_data.skills,
                        account_type: req.body.account_type ? req.body.account_type : user_data.account_type,
                        status: req.body.status ? req.body.status : user_data.status,
                        is_deleted: req.body.is_deleted ? req.body.is_deleted : user_data.is_deleted,
                        modification_notes: user_data.modification_notes
                    };
                    this.user_service.updateUser(user_params, (err: any) => {
                        //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                        if (err)
                        {
                            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_user() function : ' + err});
                            // error response if some fields are missing in request body
                            failureResponse("Getting MongoDB error", req.params.id, res)
                        }
                        //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                        //Update User data by id start Ashwin Bhayal 24/03/2021
                        else
                            successResponse('Update User successfull', null, res);
                        //Update User data by id end Ashwin Bhayal 24/03/2021
                    });
                    //Update User data by id end Ashwin Bhayal 24/03/2021
                }
                //User id is sexist end by Ashwin Bhayal 24/03/2021
                //User id does not exist start by Ashwin Bhayal 24/03/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_user() function : ' + req.params.id + " User id does not exist!"});
                    // error response if some fields are missing in request body
                    failureResponse("User id does not exist!", req.params.id, res)
                }
                //User id does not exist end by Ashwin Bhayal 24/03/2021
            });
            //Get User data by id end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in update_user() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.body, res)
        }
    }
    //Update User by id end by Ashwin Bhayal 24/03/2021

    //Delete User by id start by Ashwin Bhayal 24/03/2021
    public delete_user = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.params.id))
                throw "User id is required!";
            
            //Delete User by id start by Ashwin Bhayal 24/03/2021
            this.user_service.deleteUser(req.params.id, (err: any, delete_details) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in delete_user() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //Deleted User data by id start by Ashwin Bhayal 24/03/2021
                else if (delete_details.deletedCount !== 0)
                    successResponse('Delete User successfull', null, res);
                //Deleted User data by id end by Ashwin Bhayal 24/03/2021
                //User id does not exist start by Ashwin Bhayal 24/03/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in delete_user() function : ' + req.params.id + " User id does not exist!"});
                    // error response if some fields are missing in request body
                    failureResponse("User id does not exist!", req.params.id, res)
                }
                //User id does not exist end by Ashwin Bhayal 24/03/2021
            });
            //Delete User by id end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in delete_user() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.params.id, res)
        }
    }
    //Delete User by id end by Ashwin Bhayal 24/03/2021

    //Login User by email_address and password start by Ashwin Bhayal 24/03/2021
    public login = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.body.email_address))
                throw "Email Addess is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.email_address, 10))
                throw "Email Addess is allow to minimum 10 alphabets!";
            else if (!this.validation_helper.inputMaximumLength(req.body.email_address, 150))
                throw "Email Addess is allow to maximum 150 alphabets!";
            else if (!this.validation_helper.inputAllowOnlyEmail(req.body.email_address))
                throw "Invalid Email Addess!";
            else if (!this.validation_helper.inputRequired(req.body.password))
                throw "Password is required!";
            else if (!this.validation_helper.inputMinimumLength(req.body.password, 8))
                throw "Password is allow to minimum 8 characters!";
            else if (!this.validation_helper.inputMaximumLength(req.body.password, 25))
                throw "Password is allow to maximum 25 characters!";
            
            //Get User data by Email Address start by Ashwin Bhayal 24/03/2021
            const user_filter = { email_address: req.body.email_address };
            this.user_service.filterUser(user_filter, async(err: any, user_data: IUser) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in login() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.body.email_address, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //Get User data by Email Address start by Ashwin Bhayal 24/03/2021
                else if (!user_data.status)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in login() function : ' + req.body.email_address + " Enter the correct password!"});
                    // error response if some fields are missing in request body
                    failureResponse("Your account is deactivated!", req.body.email_address, res)
                }
                //Get User data by Email Address end by Ashwin Bhayal 24/03/2021
                //Get User data by Email Address start by Ashwin Bhayal 24/03/2021
                else if (user_data)
                {
                    //Password comapre start by Ashwin Bhayal 24/03/2021
                    await bcrypt.compare(req.body.password, user_data.password)
                    .then((result) => {
                        //Password does not matched
                        if (!result)
                        {
                            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in login() function : ' + req.body.email_address + " account is deactivated!"});
                            // error response if some fields are missing in request body
                            failureResponse("Enter the correct password!", req.body.email_address, res)
                        }
                        //password matched
                        //genrate user access token
                        user_data.access_token = jwt.sign({ userId: user_data._id, email_address: user_data.email_address, fullname: user_data.name.first_name+' '+user_data.name.last_name }, process.env.JWT_SECRET);
                        this.user_service.updateUser(user_data, (err: any) => {
                            //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                            if (err)
                            {
                                this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in login() function : ' + err});
                                // error response if some fields are missing in request body
                                failureResponse("Getting MongoDB error", req.body.email_address, res)
                            }
                            //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                        });
                        user_data.password = undefined;
                        successResponse('User logged successfull', user_data, res);
                    });
                    //Password comapre end by Ashwin Bhayal 24/03/2021
                }
                //Get User data by Email Address end by Ashwin Bhayal 24/03/2021
                //User Email Address does not exist start by Ashwin Bhayal 24/03/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in login() function : ' + req.body.email_address + " Email Address does not exist!"});
                    // error response if some fields are missing in request body
                    failureResponse("Email Address does not exist!", req.body.email_address, res)
                }
                //User Email Address does not exist end by Ashwin Bhayal 24/03/2021
            });
            //Get User data by Email Address end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in login() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.params.id, res)
        }
    }
    //Login User by email_address and password end by Ashwin Bhayal 24/03/2021

    //Logout User by email_address and password start by Ashwin Bhayal 24/03/2021
    public logout = async (req: Request, res: Response) =>
    {
        try
        {
            // this check whether all the filds were send through the erquest or not
            if (!this.validation_helper.inputRequired(req.body.access_token))
                throw "User token is required!";
            
            //Get User data by id start by Ashwin Bhayal 24/03/2021
            const user_filter = { access_token: req.body.access_token };
            this.user_service.filterUser(user_filter, (err: any, user_data: IUser) => {
                //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                if (err)
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in logout() function : ' + err});
                    // error response if some fields are missing in request body
                    failureResponse("Getting MongoDB error", req.params.id, res)
                }
                //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                //User id is exit start by Ashwin Bhayal 24/03/2021
                else if (user_data)
                {
                    //Logout User data by id start Ashwin Bhayal 24/03/2021
                    user_data.modification_notes.push({
                        modified_on: new Date(Date.now()),
                        modified_by: null,
                        modification_note: 'User logout'
                    });
                    const user_params: IUser = {
                        _id: req.params.id,
                        name: {
                            first_name: user_data.name.first_name,
                            last_name: user_data.name.last_name
                        },
                        email_address: user_data.email_address,
                        password: user_data.password,
                        dob: user_data.dob,
                        skills: user_data.skills,
                        account_type: user_data.account_type,
                        access_token: "",
                        modification_notes: user_data.modification_notes
                    };
                    this.user_service.updateUser(user_params, (err: any) => {
                        //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
                        if (err)
                        {
                            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in logout() function : ' + err});
                            // error response if some fields are missing in request body
                            failureResponse("Getting MongoDB error", req.body.access_token, res)
                        }
                        //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
                        //Logout User data by id start Ashwin Bhayal 24/03/2021
                        else
                            successResponse('User Logout successfull', null, res);
                        //Logout User data by id end Ashwin Bhayal 24/03/2021
                    });
                    //Logout User data by id end Ashwin Bhayal 24/03/2021
                }
                //User id is sexist end by Ashwin Bhayal 24/03/2021
                //User id does not exist start by Ashwin Bhayal 24/03/2021
                else
                {
                    this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in logout() function : ' + req.body.access_token + " User id does not exist!"});
                    // error response if some fields are missing in request body
                    failureResponse("User id does not exist!", req.body.access_token, res)
                }
                //User id does not exist end by Ashwin Bhayal 24/03/2021
            });
            //Get User data by id end by Ashwin Bhayal 24/03/2021
        }
        catch (error)
        {
            this.logger.logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in logout() function : ' + error});
            // error response if some fields are missing in request body
            failureResponse(error, req.body.access_token, res)
        }
    }
    //Login User by email_address and password end by Ashwin Bhayal 24/03/2021
}