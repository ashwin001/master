import * as moment from 'moment'

export class ValidationHelper {

    //Input Type required start by Ashwin Bhayal 21/04/2021
    public inputRequired(value:String = ""){
        if (value.trim().length === 0)
            return false;
        return true;
    }
    //Input Type required end by Ashwin Bhayal 21/04/2021

    //Input Type Array required start by Ashwin Bhayal 21/04/2021
    public inputEmptyArray(value:Array<String>=[]){
        if (value.length === 0)
            return false;
        return true;
    }
    //Input Type Array required end by Ashwin Bhayal 21/04/2021

    //Input Type Allow Only Alphabets start by Ashwin Bhayal 21/04/2021
    public inputAllowOnlyAlphabets(value:String = ""){
        if (!value.match(/[a-zA-Z]/i))
            return false;
        return true;
    }
    //Input Type Allow Only Alphabets end by Ashwin Bhayal 21/04/2021

    //Input Type Allow Only Alphabets and Numbers start by Ashwin Bhayal 21/04/2021
    public inputAllowOnlyAlphaNumeric(value:String = ""){
        if (!value.match(/^[a-z0-9]+$/i))
            return false;
        return true;
    }
    //Input Type Allow Only Alphabets end by Ashwin Bhayal 21/04/2021

    //Input Type Allow Only Numbers start by Ashwin Bhayal 21/04/2021
    public inputAllowOnlyNumbers(value:String = ""){
        if (!value.match(/\d/g))
            return false;
        return true;
    }
    //Input Type Allow Only Number end by Ashwin Bhayal 21/04/2021

    //Input Type Allow Only Email Address start by Ashwin Bhayal 21/04/2021
    public inputAllowOnlyEmail(value:String = ""){
        if (!value.trim().match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/))
            return false;
        return true;
    }
    //Input Type Allow Only Email Address end by Ashwin Bhayal 21/04/2021

    //Input Type Minimum Length start by Ashwin Bhayal 21/04/2021
    public inputMinimumLength(value:String = "", mLength:number = 3){
        if (value.trim().length < mLength)
            return false;
        return true;
    }
    //Input Type Minimum Length end by Ashwin Bhayal 21/04/2021

    //Input Type Maximum Length start by Ashwin Bhayal 21/04/2021
    public inputMaximumLength(value:String = "", mLength:number = 25){
        if (value.trim().length > mLength)
            return false;
        return true;
    }
    //Input Type Maximum Length end by Ashwin Bhayal 21/04/2021

    //Input Type can't be 0 start by Ashwin Bhayal 21/04/2021
    public inputCantBeZero(value:String = ""){
        if (Number(value) === 0)
            return false;
        return true;
    }
    //Input Type can't be 0 end by Ashwin Bhayal 21/04/2021

    //Password must be Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character start by Ashwin Bhayal 21/04/2021
    public inputCheckStrongPassword (value:String = ""){
        if (!value.trim().match("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,50}$"))
            return false;
        return true;
    }
    //Password must be Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character end by Ashwin Bhayal 21/04/2021

    //Input validate array data start by Ashwin Bhayal 21/04/2021
    public inputValidateData (value:String = "", validateArray:Array<String> = []){
        if (validateArray.indexOf(value) === -1)
            return false;
        return true;
    }
    //Input validate array data end by Ashwin Bhayal 21/04/2021

    //Input validate date start by Ashwin Bhayal 21/04/2021
    public inputValidateDate (value:Date){
        if (!moment(value).isValid())
            return false;
        return true;
    }
    //Input validate date end by Ashwin Bhayal 21/04/2021

    //Input validate date is less than current date start by Ashwin Bhayal 21/04/2021
    public inputValidateDateLessThanCurrentDate (start_date:Date, end_date:Date){
        if (!moment(moment(end_date).format("MM/DD/YYYY hh:mm:ss")).isAfter(moment(start_date).format("MM/DD/YYYY hh:mm:ss")))
            return false;
        return true;
    }
    //Input validate date is less than current date end by Ashwin Bhayal 21/04/2021
}