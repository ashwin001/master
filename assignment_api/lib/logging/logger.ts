import * as winston from 'winston';
import * as dotenv from "dotenv";
import * as fs from "fs";

dotenv.config({path: __dirname +'/../.env'});

export default class Logger {
    private combine = winston.format.combine;
    private label = winston.format.label;
    private timestamp = winston.format.timestamp;
    private prettyPrint = winston.format.prettyPrint;
    public logger: winston.Logger;

    constructor()
    {
        // Create the log directory if it does not exist
        if (!fs.existsSync(process.env.APP_LOGS))
            fs.mkdirSync(process.env.APP_LOGS);

        const myFormat = winston.format.printf(({ level, message, timestamp }) => {
            return `${timestamp} ${level}: ${message}`;
            });
        //Create logger object
        this.logger = winston.createLogger({
            format: this.combine(
                  this.timestamp(),
                  myFormat,
                ),
            transports: [
              new winston.transports.Console(),
              new winston.transports.File({ filename: './'+process.env.APP_LOGS+'/combined.log'  }),
            ],
            exitOnError: false,
          });
    }
} 