import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import * as dotenv from "dotenv";
import Logger from "../logging/logger";
import { IUser } from '../modules/user/model';
import UserService from '../modules/user/service';

dotenv.config({path: __dirname +'/../.env'});
async function authMiddleware(req: Request, res: Response, next: NextFunction)
{
    try
    {
        //Get the jwt token from the head
        const token = <string>req.headers["auth"];
        //token not sent in header
        if (!token)
        {
            new Logger().logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in authMiddleware function : token not send'});
            return res.status(401).json({ 'status': 'error', 'message': 'Token is not sent in header!' });
        }
        const verificationResponse = <any>jwt.verify(token, process.env.JWT_SECRET);
        const user_service = new UserService();
        const { userId } = verificationResponse;
        const user_filter = { _id: userId };
        user_service.filterUser(user_filter, (err: any, user_data: IUser) => {
            //Getting MongoDB error start by Ashwin Bhayal 24/03/2021
            if (err)
                throw "Getting MongoDB error!";
            //Getting MongoDB error end by Ashwin Bhayal 24/03/2021
            //Get User data by id start by Ashwin Bhayal 24/03/2021
            else if (!user_data)
                throw "User does not exist!"
            //Get User data by id end by Ashwin Bhayal 24/03/2021
            //User id does not exist start by Ashwin Bhayal 24/03/2021
            else if (user_data.access_token != token)
                throw "User token does not match!"
            //User id does not exist end by Ashwin Bhayal 24/03/2021
        });
        //Get User data by id end by Ashwin Bhayal 24/03/2021
        next();
    }
    catch (e)
    {
        new Logger().logger.log({level: 'error', message: __filename.slice(__dirname.length + 1) + ' file in authMiddleware function : '+e.message});
        return res.status(401).json({ 'status': 'error', 'message': e.message });
    }
}

export default authMiddleware;