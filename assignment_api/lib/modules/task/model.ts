import { ModificationNote } from "../common/model";

export interface ITask {
    _id?: String;
    title: String;
    description: String;
    due_date: Date;
    skills: Array<String>;
    assign_to: Array<String>;
    status?: Boolean;
    is_deleted?: Boolean;
    created_at?: Date;
    modification_notes: ModificationNote[]
}