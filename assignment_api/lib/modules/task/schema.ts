import * as mongoose from 'mongoose';
import { ModificationNote } from '../common/model';

const Schema = mongoose.Schema;

const schema = new Schema({
    title: {
        type: String,
        trim: true,
        required: true,
        unique: true,
        min: 3,
        max: 150,
    },
    description: {
        type: String,
        trim: true,
        required: true,
    },
    due_date: {
        type: Date,
        trim: true,
        required: true,
    },
    skills: {
        type: Array,
        trim: true,
        require: true,
    },
    assign_to: {
        type: Array,
        trim: true,
        require: true,
    },
    status: {
        type: Boolean,
        default: true
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
    modification_notes: [ModificationNote]
});

export default mongoose.model('tasks', schema);