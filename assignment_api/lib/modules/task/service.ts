import { ITask } from './model';
import tasks from './schema';

export default class TaskService {    
    //Create a new Task
    public createTask(task_params: ITask, callback: any) {
        const _session = new tasks(task_params);
        _session.save(callback);
    }

    //Get All Task
    public getAllTask(query: any, callback: any) {
        tasks.find(query, callback);
    }

    //Filter Task by column name
    public filterTask(query: any, callback: any) {
        tasks.findOne(query, callback);
    }

    //Update Task by id
    public updateTask(task_params: ITask, callback: any) {
        const query = { _id: task_params._id };
        tasks.findOneAndUpdate(query, task_params, callback);
    }
    
    //Delete Task by id
    public deleteTask(_id: String, callback: any) {
        const query = { _id: _id };
        tasks.deleteOne(query, callback);
    }
}