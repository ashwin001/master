import { ModificationNote } from "../common/model";

export interface IUser {
    _id?: String;
    name: {
        first_name: String;
        last_name: String;
    };
    email_address: String;
    password: String;
    dob: Date;
    account_type: Number;
    skills: Array<String>;
    status?: Boolean;
    access_token?: String;
    is_deleted?: Boolean;
    created_at?: Date;
    modification_notes: ModificationNote[]
}

export interface IUserGet {
    _id?: String;
    name: {
        first_name: String;
        last_name: String;
    };
    email_address: String;
    password: String;
    dob: Date;
    account_type: Number;
    skills: Array<String>;
    status?: Boolean;
    is_deleted?: Boolean;
    created_at?: Date;
    modification_notes: ModificationNote[]
}