import * as mongoose from 'mongoose';
import { ModificationNote } from '../common/model';

const Schema = mongoose.Schema;

const schema = new Schema({
    name: {
        first_name: {
            type: String,
            trim: true,
            required: true,
            min: 3,
            max: 25,
        },
        last_name: {
            type: String,
            trim: true,
            required: true,
            min: 3,
            max: 25,
        },
    },
    email_address: {
        type: String,
        unique: true,
        trim: true,
        required: true,
        min: 10,
        max: 150,
    },
    password: {
        type: String,
        trim: true,
        required: true,
    },
    dob: {
        type: Date,
        trim: true,
        required: true,
    },
    account_type: {
        type: Number,
        require: true,
        enum: [1, 2, 3]
    },
    skills: {
        type: Array,
        trim: true,
        require: true,
    },
    status: {
        type: Boolean,
        default: true
    },
    access_token: {
        type: String,
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
    modification_notes: [ModificationNote]
});

export default mongoose.model('users', schema);