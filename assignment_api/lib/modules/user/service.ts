import { IUser } from './model';
import users from './schema';

export default class UserService {    
    //Create a new User
    public createUser(user_params: IUser, callback: any) {
        const _session = new users(user_params);
        _session.save(callback);
    }

    //Get All User
    public getAllUser(query: any, callback: any) {
        users.find(query, callback);
    }

    //Filter User by column name
    public filterUser(query: any, callback: any) {
        users.findOne(query, callback);
    }

    //Update User by id
    public updateUser(user_params: IUser, callback: any) {
        const query = { _id: user_params._id };
        users.findOneAndUpdate(query, user_params, callback);
    }
    
    //Delete User by id
    public deleteUser(_id: String, callback: any) {
        const query = { _id: _id };
        users.deleteOne(query, callback);
    }
}