import {Request, Response} from "express";

export class CommonRoutes {       
    public routes(app): void {          
        app.route('/')
        .get((req: Request, res: Response) => {            
            res.status(404).send({ error: true, message: 'Check your URL please' })
        })               
    }
}