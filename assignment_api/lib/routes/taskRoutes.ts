import {Request, Response} from "express";
import authMiddleware from "../middleware/auth.middleware"
import { TaskController } from '../controllers/taskController';

export class TaskRoutes {

    private task_controller: TaskController = new TaskController();
    
    public routes(app): void {
        
        // Get All Task start by Ashwin Bhayal 21/04/2021
        app.route('/tasks')
        .get(authMiddleware, (req: Request, res: Response) => {
            this.task_controller.get_tasks(req, res);
        })
        // Get All Task end by Ashwin Bhayal 21/04/2021
        
        //Create New Task start by Ashwin Bhayal 21/04/2021
        app.route('/task')
        .post(authMiddleware, (req: Request, res: Response) => {
            this.task_controller.create_task(req, res);
        })
        //Create New Task end by Ashwin Bhayal 21/04/2021

        // Task detail by id start by Ashwin Bhayal 21/04/2021
        app.route('/task/:id')
        // Get Task detail start by Ashwin Bhayal 21/04/2021
        .get(authMiddleware, (req: Request, res: Response) => {
            this.task_controller.get_task(req, res);
        })
        // Get Task detail end by Ashwin Bhayal 21/04/2021
        // Update Task detail start by Ashwin Bhayal 21/04/2021
        .put(authMiddleware, (req: Request, res: Response) => {
            this.task_controller.update_task(req, res);
        })
        // Update Task detail end by Ashwin Bhayal 21/04/2021
        // Delete Task detail start by Ashwin Bhayal 21/04/2021
        .delete(authMiddleware, (req: Request, res: Response) => {
            this.task_controller.delete_task(req, res);
        })
        // Delete Task detail end by Ashwin Bhayal 21/04/2021
        // Task detail by id start by Ashwin Bhayal 21/04/2021
    }
}