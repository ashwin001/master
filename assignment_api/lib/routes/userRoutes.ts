import {Request, Response} from "express";
import authMiddleware from "../middleware/auth.middleware"
import { UserController } from '../controllers/userController';

export class UserRoutes {

    private user_controller: UserController = new UserController();
    
    public routes(app): void {
        
        //User login start by Ashwin Bhayal 21/04/2021
        app.route('/login')
        .post((req: Request, res: Response) => {
            this.user_controller.login(req, res);
        })
        //User login end by Ashwin Bhayal 21/04/2021
        
        //User logout start by Ashwin Bhayal 21/04/2021
        app.route('/logout')
        .post(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.logout(req, res);
        })
        //User logout end by Ashwin Bhayal 21/04/2021
        
        // Get All User start by Ashwin Bhayal 21/04/2021
        app.route('/users')
        .get(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.get_users(req, res);
        })
        // Get All User end by Ashwin Bhayal 21/04/2021
        
        // Get All User start by Ashwin Bhayal 21/04/2021
        app.route('/users')
        .post(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.get_users_account_type(req, res);
        })
        // Get All User end by Ashwin Bhayal 21/04/2021
        
        //Create New User start by Ashwin Bhayal 21/04/2021
        app.route('/user')
        .post(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.create_user(req, res);
        })
        //Create New User end by Ashwin Bhayal 21/04/2021

        // User detail by id start by Ashwin Bhayal 21/04/2021
        app.route('/user/:id')
        // Get User detail start by Ashwin Bhayal 21/04/2021
        .get(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.get_user(req, res);
        })
        // Get User detail end by Ashwin Bhayal 21/04/2021
        // Update User detail start by Ashwin Bhayal 21/04/2021
        .put(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.update_user(req, res);
        })
        // Update User detail end by Ashwin Bhayal 21/04/2021
        // Delete User detail start by Ashwin Bhayal 21/04/2021
        .delete(authMiddleware, (req: Request, res: Response) => {
            this.user_controller.delete_user(req, res);
        })
        // Delete User detail end by Ashwin Bhayal 21/04/2021
        // User detail by id start by Ashwin Bhayal 21/04/2021
    }
}