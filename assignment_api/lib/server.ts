import app from "./app";
import * as dotenv from "dotenv";

dotenv.config({path: __dirname +'/../.env'});
const PORT = process.env.APP_PORT;

app.listen(PORT, () => {
    console.log('Express server listening on port ' + PORT);
})